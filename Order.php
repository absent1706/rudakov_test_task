<?php

/**
 * Product class
 */
class Product
{
    /**
     * Product code
     *
     * @var string
     */
    protected $_code;

    /**
     * Product price
     *
     * @var float
     */
    protected $_price;

    public function __construct($code, $price)
    {
        $this->_code  = $code;
        $this->_price = $price;
    }

    public function setPrice($price)
    {
        $this->_price = $price;
    }

    public function getPrice()
    {
        return $this->_price;
    }

    public function getCode()
    {
        return $this->_code;
    }
}

/**
 * Product collection class
 */
class ProductCollection
{
    /**
     * All collection products
     *
     * @var array
     */
    protected $_products = array();

    /**
     * Add product to collection
     *
     * @param Product $product
     */
    public function add(Product $product)
    {
        $this->_products[] = $product;
    }

    /**
     * Get products. If $productCount is set, return first $productCount products
     *
     * @param  int $productCount
     *
     * @return array
     */
    public function getProducts($productCount = null)
    {
        if ($productCount && ($productCount <= $this->getProductCount()))
        {
            $result = array_slice($this->_products, 0, $productCount, true);
        }
        else
        {
            $result = $this->_products;
        }

        return $result;
    }

    public function getProductById($id)
    {
        return isset($this->_products[$id]) ? $this->_products[$id] : null;
    }

    /**
     * Does the same as getProducts(), but returns only product ids
     *
     * @param  int $productCount
     *
     * @return array
     */
    public function getProductIds($productCount = null)
    {
        return array_keys($this->getProducts($productCount));
    }

    /**
     * Get all product codes
     *
     * @return array
     */
    public function getProductCodes()
    {
        $result = array();
        foreach ($this->_products as $product)
        {
            $result[] = $product->getCode();
        }

        return $result;
    }

    /**
     * Gets assoc array like <product id> => <product code>
     *
     * @return array <product 1 id> => <product 1 code>, ...
     */
    public function getProductIdsAndCodes()
    {
        $result = array();
        foreach ($this->_products as $productId => $product)
        {
            $result[$productId] = $product->getCode();
        }

        return $result;
    }

    /**
     * Gets total product number
     *
     * @return int
     */
    public function getProductCount()
    {
        return count($this->_products);
    }

    /**
     * Excludes product with given ids from collection
     *
     * @param  array $productIds
     */
    public function excludeProductsById($productIds)
    {
        if (!is_array($productIds))
        {
            $productIds = array($productIds);
        }

        foreach ($productIds as $productId)
        {
            if (array_key_exists($productId, $this->_products))
            {
                unset($this->_products[$productId]);
            }
        }
    }

    /**
     * Excludes product with given codes from collection
     *
     * @param  array $productCodes
     */
    public function excludeProductsByCode($productCodes)
    {
        $idsAndCodes = $this->getProductIdsAndCodes();

        $idsToExclude = array();
        foreach ($productCodes as $code)
        {
            $idsToExclude = array_merge($idsToExclude, array_keys($idsAndCodes, $code));
        }

        $this->excludeProductsById($idsToExclude);
    }

    /**
     * Gets positions (ids) of products in given set
     * Returns result only if ALL products in set were found
     * Only first position of each product is returned
     *
     * @param  array $productSetCodes
     *
     * @return array product ids (positions in collection)
     */
    public function matchProductSetByCodes($productSetCodes)
    {
        if (!is_array($productSetCodes))
        {
            $productSetCodes = array($productSetCodes);
        }

        $idsAndCodes = $this->getProductIdsAndCodes();

        $matches = array_unique(array_intersect($idsAndCodes, $productSetCodes));

        return (count($matches) == count($productSetCodes)) ? array_keys($matches) : array();
    }

    /**
     * Returns position (id) of firstly occured product from given set of codes
     *
     * @param  array $productsCodes
     *
     * @return int|null
     */
    public function matchOneProductOfSetByCodes($productsCodes)
    {
        $idsAndCodes = $this->getProductIdsAndCodes();

        if ($matches = array_intersect($idsAndCodes, $productsCodes))
        {
            $keys = array_keys($matches);
            $result = reset($keys);
        }
        else
        {
            $result = null;
        }

        return $result;
    }
}

/**
 * Order class
 */
class Order
{
    /**
     * Price calculator that order uses
     *
     * @var PriceCalculator
     */
    protected $_priceCalculator;

    /**
     * Collection of bought products
     *
     * @var ProductCollection
     */
    protected $_products;

    public function setPriceCalculator(PriceCalculator $priceCalculator)
    {
        $this->_priceCalculator = $priceCalculator;
        return $this;
    }

    public function setProducts(ProductCollection $productCollection)
    {
        $this->_products = $productCollection;
        return $this;
    }

    public function getProducts()
    {
        return $this->_products;
    }

    /**
     * Gets total price for bought products. Calls getTotalPrice() function of price calculator
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->_priceCalculator->setProductList($this->getProducts())->getTotalPrice();
    }
}

class PriceCalculator
{
    /**
     * Rules used for price calculation
     *
     * @var array
     */
    protected $_rules = array();

    /**
     * Products which price is calculated
     *
     * @var ProductCollection
     */
    protected $_productList;

    /**
     * List of applied rules
     *
     * @var array
     */
    protected $_appliedRules;

    public function addRule(AbstractRule $rule)
    {
        $this->_rules[] = $rule;
        return $this;
    }

    public function setProductList(ProductCollection $products)
    {
        $this->_productList = $products;
        return $this;
    }

    /**
     * Checks, whether the rule can be applied
     *
     * @param  AbstractRule $rule
     *
     * @return bool
     */
    protected function _canApplyRule(AbstractRule $rule)
    {
        $result = true;

        if ($rule instanceof GroupDiscount)
        {
            // look for types of successfully(!) applied rules. If some group discount was already successfully applied, return false
            foreach ($this->getAppliedRules(true) as $appliedRule)
            {
                if ($appliedRule instanceof GroupDiscount)
                {
                    $result = false;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Actions after rule applying
     *
     * @param  AbstractRule $rule
     */
    protected function _afterApplyRule(AbstractRule $rule)
    {
        if ($rule instanceof AbstractDiscount)
        {
            // exclude used products from current product list
            $alreadyUsedProductIds = $rule->getCondition()->getCheckResult()->getInfo('matched_product_ids');
            $this->_productList->excludeProductsById($alreadyUsedProductIds);
        }
    }

    /**
     * Calculates total price by consequently applying rules
     * For each rule, apply() function returns new price
     *
     * @return float
     */
    public function getTotalPrice()
    {
        $price = 0;
        $this->_appliedRules = array();

        foreach ($this->getRules() as $rule)
        {
            if ($this->_canApplyRule($rule))
            {
                $price = $rule->apply($this->_productList, $price);
                $this->_afterApplyRule($rule);

                $this->_appliedRules[] = $rule;
            }
        }
        return $price;
    }

    public function getRules()
    {
        return $this->_rules;
    }

    /**
     * Gets applied rules. If $onlySuccessedRules is TRUE, gets only successfully applied rules (rules whose conditions were satisfied)
     *
     * @param  boolean $onlySuccessedRules
     *
     * @return array
     */
    public function getAppliedRules($onlySuccessedRules = false)
    {
        if ($onlySuccessedRules)
        {
            $result = array();
            foreach ($this->_appliedRules as $rule)
            {
                if (($condition = $rule->getCondition()) && $condition->getCheckResult()->isSuccess())
                {
                    $result[] = $rule;
                }
            }
        }
        else
        {
            $result = $this->_appliedRules;
        }

        return $result;
    }
}

/**
 * Abstract rule
 */
abstract class AbstractRule
{
    protected $_condition;

    abstract protected function _calculatePrice($price, ProductCollection $productCollection);

    public function setCondition(AbstractCondition $condition)
    {
        $this->_condition = $condition;
    }

    public function getCondition()
    {
        return $this->_condition;
    }

    /**
     * Apply rule and recalculate price (previously checking condition if it exists)
     *
     * @param  ProductCollection $productCollection
     * @param  float             $price
     *
     * @return float
     */
    public function apply(ProductCollection $productCollection, $price)
    {
        // calculate price. if condition exists, calculate price only if condition is satisfied
        if (!$this->_condition || ($this->_condition && $this->_condition->check($productCollection)))
        {
            $price = $this->_calculatePrice($price, $productCollection);
        }

        return $price;
    }
}

/**
 * Simple rule for adding prices of all products
 */
class PriceSumRule extends AbstractRule
{
    /**
     * Adds all product prices
     *
     * @param  float             $price
     * @param  ProductCollection $productCollection
     *
     * @return float
     */
    protected function _calculatePrice($price, ProductCollection $productCollection)
    {
        $result = 0;
        foreach ($productCollection->getProducts() as $product)
        {
            $result += $product->getPrice();
        }

        return $result;
    }
}

/**
 * Abstract discount
 */
abstract class AbstractDiscount extends AbstractRule
{
    /**
     * Discount amount
     *
     * @var float
     */
    protected $_discountAmount;

    public function __construct($discountAmount, AbstractCondition $condition)
    {
        $this->setDiscountAmount($discountAmount);
        $this->setCondition($condition);
    }

    public function setDiscountAmount($discountAmount)
    {
        $this->_discountAmount = $discountAmount;
    }
}

/**
 * Discount like 'if N products (except some excluded ones) are in order, apply an P% discount on ALL order'
 */
class GroupDiscount extends AbstractDiscount
{
    /**
     * Sets discount condition. Condition should be of valid type
     *
     * @param GroupCondition $condition
     */
    public function setCondition(GroupCondition $condition)
    {
        $this->_condition = $condition;
    }

    protected function _calculatePrice($price, ProductCollection $productCollection)
    {
        return $price * (100 - $this->_discountAmount) / 100;
    }
}

class ProductSetDiscount extends AbstractDiscount
{
    /**
     * Sets discount condition. Condition should be of valid type
     *
     * @param ProductSetCondition $condition
     */
    public function setCondition(ProductSetCondition $condition)
    {
        $this->_condition = $condition;
    }

    /**
     * Apply given discount on each product matched by condition
     *
     * @param  float             $price
     * @param  ProductCollection $productCollection
     *
     * @return float
     */
    protected function _calculatePrice($price, ProductCollection $productCollection)
    {
        $matchedProductIds = $this->_condition->getCheckResult()->getInfo('matched_product_ids');
        foreach ($matchedProductIds as $id)
        {
            if ($product = $productCollection->getProductById($id))
            {
                $price -= $product->getPrice() * $this->_discountAmount / 100;
            }
        }

        return $price;
    }
}

/**
 * Discount like "if 'A' (first product) and one of ['K','L','M'] (second product) bought, apply P% discount on second product"
 */
class SecondCheaperDiscount extends AbstractDiscount
{
    /**
     * Sets discount condition. Condition should be of valid type
     *
     * @param SecondCheaperCondition $condition
     */
    public function setCondition(SecondCheaperCondition $condition)
    {
        $this->_condition = $condition;
    }

    /**
     * Apply given discount on second product (i.g., one of ['K','L','M'])
     *
     * @param  float             $price
     * @param  ProductCollection $productCollection
     *
     * @return float
     */
    protected function _calculatePrice($price, ProductCollection $productCollection)
    {
        $matchedProductIds = $this->_condition->getCheckResult()->getInfo('matched_product_ids');
        $secondProductId   = $matchedProductIds['second_product_id'];

        if ($product = $productCollection->getProductById($secondProductId))
        {
            $price -= $product->getPrice() * $this->_discountAmount / 100;
        }

        return $price;
    }
}

/**
 * Condition used in SecondCheaperDiscount class
 */
class SecondCheaperCondition extends AbstractCondition
{
    /**
     * Code of first product
     *
     * @var string
     */
    protected $_firstProductCode;

    /**
     *  Codes of products satisfying condition (products to apply discount)
     *
     * @var array
     */
    protected $_secondProductCodes;

    /**
     * Returns true, if all condition products exist in given product collection
     *
     * @param  ProductCollection $productCollection
     *
     * @return bool
     */
    public function check(ProductCollection $productCollection)
    {
        $matchedProductIds = $this->getMatchingProductIds($productCollection);
        $success           = (!empty($matchedProductIds));

        $this->_checkResult = new CheckResult($success, array('matched_product_ids' => $matchedProductIds));

        return $success;
    }

    /**
     * If first product and one of second product exist in given collection, return them.
     * Otherwise return empty array
     *
     * @param  ProductCollection $productCollection
     *
     * @return array
     */
    public function getMatchingProductIds(ProductCollection $productCollection)
    {
        $result = array();

        $currentCollection = clone $productCollection;
        $firstProductId = $currentCollection->matchProductSetByCodes($this->_firstProductCode);

        if ($firstProductId = reset($firstProductId))
        {
            $currentCollection->excludeProductsById($firstProductId);
            if ($secondProductId = $currentCollection->matchOneProductOfSetByCodes($this->_secondProductCodes))
            {
                $result = array(
                    'first_product_id'  => $firstProductId,
                    'second_product_id' => $secondProductId
                    );
            }

        }

        return $result;
    }

    public function __construct($firstProductCode, $secondProductCodes)
    {
        $this->setFirstProductCode($firstProductCode);
        $this->setSecondProductCodes($secondProductCodes);
    }

    public function setFirstProductCode($productCode)
    {
        $this->_firstProductCode = $productCode;
    }

    public function setSecondProductCodes($productCodes)
    {
        $this->_secondProductCodes = $productCodes;
    }
}

/**
 * Abstract condition
 */
abstract class AbstractCondition
{
    protected $_checkResult;

    abstract public function check(ProductCollection $productCollection);  // MUST return bool

    /**
     * Check result usually contains success flag and some additional information
     *
     * @return CheckResult
     */
    public function getCheckResult()
    {
        return $this->_checkResult;
    }
}

/**
 * Condition used in ProductSetDiscount class
 */
class ProductSetCondition extends AbstractCondition
{
    /**
     *  Codes of products satisfying condition
     *
     * @var array
     */
    protected $_productSetCodes;

    /**
     * Returns true, if all condition products exist in given product collection
     *
     * @param  ProductCollection $productCollection
     *
     * @return bool
     */
    public function check(ProductCollection $productCollection)
    {
        $matchedProductIds = $this->getMatchingProductIds($productCollection);
        $success           = (!empty($matchedProductIds));

        $this->_checkResult = new CheckResult($success, array('matched_product_ids' => $matchedProductIds));

        return $success;
    }

    /**
     * Gets ids of all products matched.
     * Match procedure: ALL sets of products with codes = _productSetCodes are matched
     *
     * If nothing was found, return empty array
     *
     * @param  ProductCollection $productCollection
     *
     * @return array
     */
    public function getMatchingProductIds(ProductCollection $productCollection)
    {
        $result = array();

        $currentCollection = clone $productCollection;
        while ($matched = $currentCollection->matchProductSetByCodes($this->_productSetCodes))
        {
            $result = array_merge($result,$matched);
            $currentCollection->excludeProductsById($matched);
        }

        return $result;
    }

    public function __construct($productCodes)
    {
        $this->setProductCodes($productCodes);
    }

    public function setProductCodes($productCodes)
    {
        $this->_productSetCodes = $productCodes;
    }
}

/**
 * Condition used in GroupDiscount class
 */
class GroupCondition extends AbstractCondition
{
    protected $_conditionProductCount;
    protected $_excludedProductCodes;

    public function __construct($conditionProductCount, $excludedProductCodes)
    {
        $this->setConditionProductCount($conditionProductCount);
        $this->setExcludedProductCodes($excludedProductCodes);
    }

    public function check(ProductCollection $productCollection)
    {
        $matchedProductIds = $this->getMatchingProductIds($productCollection);
        $success = (!empty($matchedProductIds));

        $this->_checkResult = new CheckResult($success, array('matched_product_ids' => $matchedProductIds));
        return $success;
    }

    /**
     * This function:
     * 1) excludes all products with codes in _excludedProductCodes
     * 2) if count of leftover products >= _conditionProductCount, returns first '_conditionProductCount' products.
     *    otherwise, returns empty array
     *
     * @param  ProductCollection $productCollection
     *
     * @return array
     */
    public function getMatchingProductIds(ProductCollection $productCollection)
    {
        $result = array();

        $filteredCollection = clone $productCollection;
        $filteredCollection->excludeProductsByCode($this->_excludedProductCodes);

        if ($filteredCollection->getProductCount() >= $this->_conditionProductCount)
        {
            $result = $filteredCollection->getProductIds($this->_conditionProductCount);
        }

        return $result;
    }

    public function setExcludedProductCodes($excludedCodes)
    {
        if (!is_array($excludedCodes))
        {
            $excludedCodes = array($excludedCodes);
        }

        $this->_excludedProductCodes = $excludedCodes;
    }

    public function setConditionProductCount($conditionProductCount)
    {
        $this->_conditionProductCount = $conditionProductCount;
    }
}

/**
 * Check result class. Usually contains success flag and some additional information
 */
class CheckResult
{
    /**
     * Returns check success
     *
     * @var bool
     */
    protected $_success;

    /**
     * Additional information about condition check
     *
     * @var array
     */
    protected $_info;

    public function __construct($success, $info = array())
    {
        $this->_success = $success;
        $this->_info    = $info;
    }

    /**
     * Returns true, if condition was satisfied, false otherwise
     *
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->_success;
    }

    /**
     * Gets check info. If $param is set, return only given param
     *
     * @param  int|string $param
     *
     * @return array|variant
     */
    public function getInfo($param = null)
    {
        return (isset($this->_info[$param])) ? $this->_info[$param] : $this->_info;
    }
}