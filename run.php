<?php
include 'Order.php';

$prices = array(
    'A' => 100,
    'B' => 200,
    'C' => 300,
    'D' => 400,
    'E' => 500,
    'F' => 600,
    'G' => 700,
    'H' => 800,
    'I' => 900,
    'J' => 1000,
    'K' => 1100,
    'L' => 1200,
    'M' => 1300
    );

$cartProductCodes = array('C', 'K', 'A', 'B', 'A', 'D', 'E', 'F', 'G', 'A', 'B', 'C','F','G');

$productSetRules = array(
    array(
        'set' => array('A','B'),
        'discount' => 10
        ),
    array(
        'set' => array('D','E'),
        'discount' => 5
        ),
    array(
        'set' => array('E','F','G'),
        'discount' => 5
        )
    );

$secondCheaperRules = array(
    array(
        'first_product_code' => 'A',
        'second_product_codes' => array('K', 'L', 'M'),
        'discount'      => 5,
        )
    );

$groupRules = array(
    array(
        'product_count' => 5,
        'discount'      => 20,
        'excluded_product_codes' => array('A', 'C')
        ),
    array(
        'product_count' => 4,
        'discount'      => 10,
        'excluded_product_codes' => array('A', 'C')
        ),
    array(
        'product_count' => 3,
        'discount'      => 5,
        'excluded_product_codes' => array('A', 'C')
        )
    );


$priceCalculator = new PriceCalculator;

$priceCalculator->addRule(new PriceSumRule);

foreach ($productSetRules as $ruleParams)
{
    $condition = new ProductSetCondition($ruleParams['set']);
    $rule = new ProductSetDiscount($ruleParams['discount'], $condition);
    $priceCalculator->addRule($rule);
}

foreach ($secondCheaperRules as $ruleParams)
{
    $condition = new SecondCheaperCondition($ruleParams['first_product_code'], $ruleParams['second_product_codes']);
    $rule = new SecondCheaperDiscount($ruleParams['discount'], $condition);
    $priceCalculator->addRule($rule);
}

foreach ($groupRules as $ruleParams)
{
    $condition = new GroupCondition($ruleParams['product_count'], $ruleParams['excluded_product_codes']);

    $rule = new GroupDiscount($ruleParams['discount'], $condition);
    $priceCalculator->addRule($rule);
}

// add products to cart
$cartProducts = new ProductCollection;
foreach ($cartProductCodes as $code)
{
    $cartProducts->add(new Product($code, $prices[$code]));
}

// create order
$order = new Order;
$totalPrice = $order->setProducts($cartProducts)->setPriceCalculator($priceCalculator)->getTotalPrice();
echo $totalPrice;
